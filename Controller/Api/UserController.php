<?php
class UserController extends BaseController
{
    /**
     * "/user/list" Endpoint - Get list of users
     */
    public function listAction()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];
        $arrQueryStringParams = $this->getQueryStringParams();
        if (strtoupper($requestMethod) == 'GET') {
            try {

            $record = [
            	[
            		"user_id" => 1,
				    "username" => "Bob",
				    "user_email" => "bob@gmail.com",
				    "user_status" => 0
            	],
            	[
            		"user_id" => 2,
				    "username" => "Amin",
				    "user_email" => "amin@gmail.com",
				    "user_status" => 1
            	],
            	[
            		"user_id" => 3,
				    "username" => "John",
				    "user_email" => "john@gmail.com",
				    "user_status" => 1
            	],
            	[
            		"user_id" => 4,
				    "username" => "Robert",
				    "user_email" => "robet@gmail.com",
				    "user_status" => 1
            	],
            	[
            		"user_id" => 5,
				    "username" => "Wajid",
				    "user_email" => "wajid@gmail.com",
				    "user_status" => 0
            	],
            	[
            		"user_id" => 6,
				    "username" => "Mehmood",
				    "user_email" => "mehmood@gmail.com",
				    "user_status" => 0
            	],
            	[
            		"user_id" => 7,
				    "username" => "Singh",
				    "user_email" => "singh@gmail.com",
				    "user_status" => 0
            	],
            ];

            $responseData = json_encode($record);
            } catch (Error $e) {
                $strErrorDesc = $e->getMessage().'Something went wrong! Please contact support.';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }
 
        // send output
        if (!$strErrorDesc) {
            $this->sendOutput(
                $responseData,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
        } else {
            $this->sendOutput(json_encode(array('error' => $strErrorDesc)), 
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }
}